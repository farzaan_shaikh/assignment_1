# Assignment 1 - Student Database Management

### Introduction
The first assignment given in order to familiarise with Django.
This project performs basic database operations, namely fetch, add, delete and update. The project is built on Postgresql and Django (v1.11.4).

### Prerequisites
Basic requirements:

* [Python 2.7](https://www.python.org/downloads/)
* [Postman](https://www.getpostman.com/)
* [Django](https://www.djangoproject.com/)
* [PostgreSQL](https://www.djangoproject.com/)

Also helpful:

* [PostgreSQL App](https://postgresapp.com/)

### Installation
- `pip install django==1.11.14`
- `pip install psycopg2`

##### MacOSX
- `brew install postgresql`

##### Linux (Debian)
- `sudo apt install postgresql`

### Running the code
- Change directory to the project folder.
- Run command `python manage.py runserver`
- To view data or modify using admin:
    - Open URL on browser `http://127.0.0.1:8000/admin`
    - username: `farzaan`
    - password: `ggwpggwp`
- To insert a new entry:
    - Paste this URL in postman `http://127.0.0.1:8000/internship/student/`
    - Select *POST* and add values corresponding to the following keys:
        - `fname` for First name
        - `lname` for Last name
        - `mob` for Mobile number
        - `add` for Address
        - `clg` for College
- To display database entries just hit the url `http://127.0.0.1:8000/internship/student/`
- To delete an entry:
    - Paste this URL in postman `http://127.0.0.1:8000/internship/student/x/`
    - Select *DELETE* and replace value of x. Here x corresponds to the enrollment number of the entry you want to delete.
- To update an entry:
    - Paste this URL in postman `http://127.0.0.1:8000/internship/student/x/`
    - Select *PUT* and replace the value of x with the enrollment number you want to modify
    - Only specify the fields you want to update.
