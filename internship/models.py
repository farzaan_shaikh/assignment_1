# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Table Students stores records in postgresql database
class Students (models.Model):
    roll_no = models.AutoField(primary_key = True)
    first_name = models.CharField(max_length = 20)
    last_name = models.CharField(max_length = 20)
    mobile_no = models.CharField(max_length=14, unique = True)
    address = models.CharField(max_length=140)
    college = models.CharField(max_length=50)
