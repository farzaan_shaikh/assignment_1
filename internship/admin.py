# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Students

class StudentsAdmin(admin.ModelAdmin):
    list_display = ('roll_no', 'first_name', 'last_name', 'mobile_no', 'address', 'college')

    def student_info(self, obj):
        return obj.description

    def get_queryset(self, request):
        queryset = super(StudentsAdmin, self).get_queryset(request)
        queryset = queryset.order_by('roll_no')
        return queryset

    student_info.short_description = 'Info'

admin.site.register(Students, StudentsAdmin)
