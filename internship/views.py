# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import View
from django.http import QueryDict
from django.http import HttpResponse
from django.db import connection

from .models import Students
from .decorators import update_validator, delete_validator, post_validator

'''The Calculator Class takes two vairiables p1, p2 as input and outputs four
arithmatic operations: Addition, Subtraction, Multiplication, Division.'''
class Calc (View):
    # Gets the variables p1&p2 and responds with the four arithmatic operations
    def get(self, request):

        try:
            p1 = float(request.GET.get('p1'))
            p2 = float(request.GET.get('p2'))
        except TypeError:
            return HttpResponse('Please enter valid arguments', status = 500)
        p3 = p1 + p2
        response = 'Sum is: ' + str(p3)
        p4 = p1 - p2
        response = response + ' Difference is: ' + str(p4)
        p5 = p1 * p2
        response = response + ' Product is: '+ str(p5)
        p6 = p1 / p2
        response = response + ' Division result: '+ str(p6)

        return HttpResponse(response, status = 200)

''' The StudentData class performs operations on the model class Students'''
class StudentData (View):
    # Fetches all stored entries in table "Students"and displays in html format
    # Total db calls = 1
    def get(self, request):

        result = []
        students = Students.objects.all()
        html = []

        htm_format = '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td>'
        htm_format = htm_format + '<td>%s</td><td>%s</td></tr>'

        html.append(htm_format %('Roll No', 'First Name', 'Last Name', 'Mobile No','Address', 'College'))
        for student in students:
            html.append(htm_format %(student.roll_no,student.first_name,student.last_name,student.mobile_no,student.address,student.college))

        html.append('Total db calls = %s'% str(len(connection.queries)))

        return HttpResponse('<table>%s</table>' % '\n'.join(html),status=200)

    # Adds an entry to the database given all fields are provided by end-user
    # Total db calls = 1
    @post_validator
    def post(self, request):
        body = request.POST

        fname = body['fname']
        lname = body['lname']
        mob = body['mob']
        add = body['add']
        clg = body['clg']

        sobj = Students(first_name = fname, last_name = lname, mobile_no = mob,
        address = add, college = clg)

        sobj.save()
        response = []
        response.append('Entry Added !')
        response.append('Total db calls = %s'% str(len(connection.queries)))
        return HttpResponse('%s' % '\n'.join(response), status = 200)


    # Deletes a tuple based on a given primary key
    # Total db calls = 2
    @delete_validator
    def delete(self, request, roll):
        sobj = Students.objects.get(pk = roll)
        sobj.delete()
        db_calls = str(len(connection.queries))
        return HttpResponse('Deletion Successful ! Total db calls = %s' %'\n'.join(db_calls), status = 200)

    # Updates an entry where providing the primary key is a must
    # Total db calls = 2
    @update_validator
    def put(self, request, roll):

        params = QueryDict(request.body)
        sobj = Students.objects.get(pk = roll)
        #sobj = Students.objects.filter(pk = roll)
        response = 'Successfully Updated: '

        if 'fname' in params.keys():
            val = params['fname']
            sobj.first_name = val
            response = response + 'First name, '
        if 'lname' in params.keys():
            val = params['lname']
            sobj.last_name = val
            response = response + 'Last name, '
        if 'mob' in params.keys():
            val = params['mob']
            sobj.mobile_no = val
            response = response + 'Mobile number, '
        if 'add' in params.keys():
            val = params['add']
            sobj.address = val
            response = response + 'Address, '
        if 'clg' in params.keys():
            val = params['clg']
            sobj.college = val
            response = response + 'College '

        sobj.save()
        response = response + 'Total db calls = %s' % str(len(connection.queries))
        return HttpResponse(response, status = 200)
