from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
from django.http import QueryDict
from django.db import IntegrityError, DataError
from django.http import HttpResponse

# Checks for valid inputs for update queries
def update_validator(function):
    def wrapper(self, *args, **kwargs):
        try:
            number = QueryDict(args[0].body)
            number = number['mob']
            if '+' in number:
                number = number.split('+')[1]
                if number.isdigit() and len(number) == 12 or len(number) == 13:
                    return function(self, *args, **kwargs)
                else:
                    return HttpResponse('Please enter a valid mobile number !',
                    status = 500)
            else:
                if number.isdigit() and len(number) == 10:
                    return function(self, *args, **kwargs)
                else:
                    return HttpResponse('Please enter a valid Mobile number !',
                    status = 500)
        except ObjectDoesNotExist:
            return HttpResponse('Error updating tuple: Object does not exist',
            status = 500)
        except MultiValueDictKeyError:
            return HttpResponse('Error updating tuple: MiltiValueDictKeyError',
            status = 500)
        except DataError:
            return HttpResponse('Data Error: Value too long',
            status = 500)
        except IntegrityError:
            return HttpResponse('Error updating: please resolve conflict',
            status = 500)

    return wrapper

# Checks for valid inputs for delete queries
def delete_validator(function):
    def wrapper(self, *args, **kwargs):
        try:
            return function(self, *args, **kwargs)
        except ObjectDoesNotExist:
            return HttpResponse('Error deleting tuple: Object does not exist',
            status = 500)
        except MultiValueDictKeyError:
            return HttpResponse('Error deleting tuple: MiltiValueDictKeyError',
            status = 500)

    return wrapper

# Checks for valid inputs for a new entry to be added into the database
def post_validator(function):
    def wrapper(self, *args):
        try:
            # number = QueryDict(*args[0].body)
            # number = number['mob']
            number = args[0].POST
            number = number['mob']
            if '+' in number:
                number = number.split('+')[1]
                if number.isdigit() and len(number) == 12 or len(number) == 13:
                    return function(self, *args)
                else:
                    return HttpResponse('Please enter a valid mobile number !',
                    status = 500)
            else:
                if number.isdigit() and len(number) == 10:
                    return function(self, *args)
                else:
                    return HttpResponse('Please enter a valid Mobile number !',
                    status = 500)
        except IntegrityError:
            return HttpResponse('Error adding entry: please resolve conflict',
            status = 500)
        except MultiValueDictKeyError:
            return HttpResponse('Error adding entry: MultiValueDictKeyError',
            status = 500)
        except DataError:
            return HttpResponse('Error adding entry: Value too long',
            status = 500)
    return wrapper
