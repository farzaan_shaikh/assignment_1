from django.conf.urls import include, url
from views import Calc, StudentData

urlpatterns = [
    url(r'^calc/', Calc.as_view(), name = 'index'),
    url(r'^student/$', StudentData.as_view(), name = 'general'),
    url(r'^student/(?P<roll>\d+)/$', StudentData.as_view(), name = 'spec')
]
